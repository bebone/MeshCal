<?php
$ret='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">'."\n";
$ret.='<?xml version="1.0" encoding="UTF-8"?>'."\n";
$ret.='<html xmlns="http://www.w3.org/1999/xhtml">'."\n";
$ret.='<head>'."\n";
$ret.='<title>MeshCal</title>'."\n";
$ret.='<!-- M�tadon� de d�scription !!! -->'."\n";
$ret.='<meta name="description" content="description en prose" />'."\n";
$ret.='<meta name="keywords" lang="fr" content="mots, clefs" />'."\n";
$ret.='<meta name="identifier-url" content="http://nom_du_site.fr/" />'."\n";
$ret.='<meta name="robots" content="index,follow" />'."\n";
$ret.='<meta http-equiv="content-language" content="fr" />'."\n";
$ret.='<meta name="Language" content="French" />'."\n";
$ret.='<meta name="coverage" content="France" />'."\n";
$ret.='<meta name="geo.country" content="fr" />'."\n";
$ret.='<meta name="author" content="J�r�me LEIGNADIER-PARADON, leiopar@free.fr" />'."\n";
$ret.='<meta name="category" content="categorie, celons, clasification, des, annuaires, en, ligne" />'."\n";
$ret.='<meta name="MSSmartTagsPreventParsing" content="TRUE" />'."\n";
$ret.='<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>'."\n";
$ret.='<link rel="shortcut icon" href="<?php echo $theme; ?>favicon.png" /> <!-- image � creer !!! -->'."\n";
$ret.='<link rel="stylesheet" href="'.$theme .'base.css" type="text/css" />'."\n";
$ret.='<link rel="stylesheet" href="'.$theme.'popup.css" type="text/css" /> <!--media="screen"-->'."\n";

$ret.='<link rel="shortcut icon" href="favicon.ico" />'."\n";
$ret.='<link rel="icon" type="image/x-icon" href="favicon.ico" />'."\n";
$ret.='<link rel="icon" type="image/png" href="meshcal_icon16x16.png"  sizes="16x16" />'."\n";
$ret.='<link rel="icon" type="image/png" href="meshcal_icon32x32.png"  sizes="32x32" />'."\n";
$ret.='<link rel="icon" type="image/png" href="meshcal_icon64x64.png"  sizes="64x64" />'."\n";

$ret.='<link rel="stylesheet" href="'.$rep_cal.'couleurs_cal.css" type="text/css" />'."\n";
$ret.='</head>'."\n";
$ret.='<body>'."\n";

return $ret;
?>