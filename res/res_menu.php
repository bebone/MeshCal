<?php

$menu=array();
$menu["J"]=date("j");
$menu["S"]=date("W");
$menu["M"]=date("n");
$menu["A"]=$tab_req[1];
$menu["timestamp"]=mktime(0, 0, 0, $menu["M"], $menu["J"], $menu["A"]);
$js=date("w", $menu["timestamp"]);
$menu["JS"]=($js==0)?7:$js;
if($tab_req[0]=="S"){ // si semaine
	$menu["S"]=$tab_req[2];
	if(isset($tab_req[3])){
		$menu["JS"]=$tab_req[3];
	}else{
		$jour_de_semaine=mktime(0, 0, 0, $menu["M"], $menu["J"], $menu["A"]);
		$menu["JS"]=(date("w", $jour_de_semaine)==0?7:date("w", $jour_de_semaine));// jour/semaine mis en avant
	}
	// redefinir du jour
	$date=week_dates($menu["S"],$menu["A"],$menu["JS"]); //date du jour de semaine en timestamp
	$menu["J"]=date("j",$date);
	$menu["M"]=date("n",$date);
}else{ //sinon (jour du mois; mois; année)
	if(isset($tab_req[2]))$menu["M"]=$tab_req[2];
	if(isset($tab_req[3])){
		$menu["J"]=$tab_req[3];
		$js=date("w", mktime(0, 0, 0, $menu["M"], $menu["J"], $menu["A"]));
		$menu["JS"]=($js==0)?7:$js;
	}
	
	for($i=0; $i<3; $i++){
        if(!checkdate( $menu["M"] , $menu["J"] , $menu["A"] )){
            $menu["J"]--;
        }else{$i=3;}
    }
	
	$menu["S"]=date("W", mktime(0, 0, 0, $menu["M"], $menu["J"], $menu["A"]));
}



// préremplisage jours
$dernier_jour_du_mois=date("j", strtotime('last day of this month', mktime(0, 0, 0, $menu["M"], 1, $menu["A"])));

for($i=1;$i<$menu["J"] && ($i<25);$i++)$menu["J1"][$menu["J"]-$i]= res_menu_date2jour($menu["J"]-$i, $menu["M"], $menu["A"])."&nbsp;".($menu["J"]-$i);
for($i=1;($i<=$dernier_jour_du_mois-$menu["J"]) && ($i<25);$i++)$menu["J2"][$menu["J"]+$i]=res_menu_date2jour($menu["J"]+$i, $menu["M"], $menu["A"])."&nbsp;".($menu["J"]+$i);

// préremplisage mois...
for($i=1;$i<$menu["M"];$i++)$menu["M1"][$menu["M"]-$i]=res_menu_num2mois($menu["M"]-$i);
for($i=$menu["M"]+1;$i<=12;$i++)$menu["M2"][$i]=res_menu_num2mois($i);

// préremplisage années
for($i=1;$i<=5;$i++){
	$menu["A1"][$menu["A"]-$i]=$menu["A"]-$i;
	$menu["A2"][$menu["A"]+$i]=$menu["A"]+$i;
}


// préremplisage semaines...
$premier_jour_de_l_annee=mktime(0, 0, 0, 1, 1, $menu["A"]);
$j1_s=(date("w", $premier_jour_de_l_annee)==0?7:date("w", $premier_jour_de_l_annee));

if ($j1_s<=4){//semaine 01 commence l'année dernière (comme 2015)
	$lundi_s0=strtotime("+".(8-$j1_s)." day", $premier_jour_de_l_annee);
	for($i=1;($i<$menu["S"]-1)&&($i<25);$i++)$menu["S1"][$menu["A"]."-".($menu["S"]-$i)]=$menu["S"]-$i;
	if($i<25)$menu["S1"][$menu["A"]."-"."1"]="#1"; //à chevale sur l'année dernier, donc en couleur d'horison (grisé)
	
}else{//semaine 01 commence cette année (comme 2012)
	$lundi_s0=strtotime("-".($j1_s-1)." day", $premier_jour_de_l_annee);
	for($i=1;($i<$menu["S"])&&($i<25);$i++)$menu["S1"][$menu["A"]."-".($menu["S"]-$i)]=$menu["S"]-$i;
	// donc afficher semaine 51, 52 ou 53 de l'année précédante pour compléter
	if ($i<25){
		$dernier_jour_de_l_annee_precedente=mktime(0, 0, 0, 12, 31, $menu["A"]-1);
		if (date("w", $dernier_jour_de_l_annee_precedente)!=0){
			$ns=date("W",$dernier_jour_de_l_annee_precedente);
			$menu["S1"][($menu["A"]-1)."-".$ns]="#".$ns;
		}
	}	
}		








// derterminer le numero de la dernière semaine	
$dernier_jour_de_l_annee=mktime(0, 0, 0, 12, 31, $menu["A"]);
$ns31_12=date("W",$dernier_jour_de_l_annee)+0;
$ns=($ns31_12!=1)?$ns31_12:0+date("W",mktime(0, 0, 0, 12, 24, $menu["A"]));
for($i=1;($i<=($ns-$menu["S"]))&&($i<25);$i++)$menu["S2"][$menu["A"]."-".($menu["S"]+$i)]=$menu["S"]+$i;
if ($i<25)	if ($ns31_12==$ns)$menu["S2"][$menu["A"]."-S1"]="#S1";






/* echo "<pre>";
print_r($menu);
echo "</pre><hr>"; */

function res_menu($type){
	global $tab_req, $menu, $traiter_cal, $dernier_jour_du_mois;
	setlocale(LC_TIME, 'fr', 'fr_FR.UTF-8'); /* à internationaliser...*/
	
	//echo "[".($traiter_cal?"vrai":"faux")."]";
	
	/* $entete.='<b>'.strftime('%A %d %B %Y', mktime(0, 0, 0, $menu["M"], $menu["J"], $menu["A"]))." (S".date("W").")</b>"; */
	$ret=$type;
	if($type=="J")$ret=($tab_req[0]=="J" && $traiter_cal)?res_menu_date2jour($menu["J"], $menu["M"], $menu["A"])."&nbsp".$menu["J"]:'<a href="?aff=J-'.$menu["A"].'-'.$menu["M"].'-'.$menu["J"].'">'.res_menu_date2jour($menu["J"], $menu["M"], $menu["A"])."&nbsp".$menu["J"].'</a>';	/*** le jour existe t-il ? (29 fevier, 31 avril ... ) */ /* ajouter jour de la semaine */
	if($type=="S")$ret=($tab_req[0]=="S" && $traiter_cal)?'S'.$menu["S"]:'<a href="?aff=S-'.$menu["A"].'-'.$menu["S"].'-'.$menu["J"].'">'.'S'.$menu["S"].'</a>';	
	if($type=="M")$ret=($tab_req[0]=="M" && $traiter_cal)?res_menu_num2mois($menu["M"]):'<a href="?aff=M-'.$menu["A"].'-'.$menu["M"].'-'.$menu["J"].'">'.res_menu_num2mois($menu["M"]).'</a>';
	if($type=="A")$ret=($tab_req[0]=="A" && $traiter_cal)?$menu["A"]:'<a href="?aff=A-'.$menu["A"].'">'.$menu["A"].'</a>';



	if($type=="<J"){ //les jours précédents...
		$ret="";
		if(isset($menu["J1"]))if($menu["J"]>1)
		$ret=res_menu_tab(
			array(
				"label" => "&larr;",
				"url_debut" => "?aff=J-".$menu["A"]."-".$menu["M"]."-",
				"url_fin" => "&",
			), $menu["J1"]
		);
	}

	if($type=="J>"){ //les jours suivants...
		$ret="";
		if(isset($menu["J2"]))if($menu["J"]<$dernier_jour_du_mois)
		$ret=res_menu_tab(
			array(
				"label" => "&rarr;",
				"url_debut" => "?aff=J-".$menu["A"]."-".$menu["M"]."-",
				"url_fin" => "&",
			), $menu["J2"]
		);
	}	
	
	if($type=="<S"){ //les semaines précédents...
		$ret="";
		if(isset($menu["S1"]))if($menu["S"]>1)
		$ret=res_menu_tab(
			array(
				"label" => "&larr;",
				"url_debut" => "?aff=S-",
				"url_fin" => "-".$menu["JS"]."&",
				"prefix" => "S"
			), $menu["S1"]
		);
	}	
	
	if($type=="S>"){ //les semaines suivantes...
		$ret="";
		if(isset($menu["S2"]))if(count($menu["S2"])>0)
		$ret=res_menu_tab(
			array(
				"label" => "&rarr;",
				"url_debut" => "?aff=S-",
				"url_fin" => "-".$menu["JS"]."&",
				"prefix" => "S"
			), $menu["S2"]
		);
	}	

	if($type=="<M"){ //les mois précédents...
		$ret="";
		if($menu["M"]>1)
		$ret=res_menu_tab(
			array(
				"label" => "&larr;",
				"url_debut" => "?aff=M-".$menu["A"]."-",
				"url_fin" => "-".$menu["J"]."&"
			), $menu["M1"]
		);
	}
	
	if($type=="M>"){ //les mois suivants...
		$ret="";
		if($menu["M"]<12)
		$ret=res_menu_tab(
			array(
				"label" => "&rarr;",
				"url_debut" => "?aff=M-".$menu["A"]."-",
				"url_fin" => "-".$menu["J"]."&"
			), $menu["M2"]
		);
	}
	
	if($type=="<A"){ //les années précédantes...
		$ret=res_menu_tab(
			array(
				"label" => "&larr;",
				"url_debut" => "?aff=A-",
				"url_fin" => "&"
			), $menu["A1"]
		);
	}
	
	if($type=="A>"){ //les années suivantes...
		$ret=res_menu_tab(
			array(
				"label" => "&rarr;",
				"url_debut" => "?aff=A-",
				"url_fin" => "&"
			), $menu["A2"]
		);
	}
	
	return $ret;
}



// création html des menu... //
function res_menu_tab($t, $tab){
	$prefix=isset($t["prefix"])?$t["prefix"]:"";
	
	$ret='<ul class="menu_deroulant">
  <li class="menu_deroulant">
    '.$t["label"].'
    <ul class="menu_deroulant_2">';

	foreach($tab as $url => $label){
		$horison=(substr($label,0,1)=="#")?' class="horison"':"";
		$ret.='<li>&nbsp;<a href="'.$t["url_debut"].$url.$t["url_fin"].'"'.$horison.'>'.$prefix.trim($label,"#").'</a>&nbsp;</li>';
	}
	
    $ret.='    </ul>
  </li>
</ul>';

return $ret;
}

///////////////////////////////////////////////
// converti un numero de mois en nom de mois //
///////////////////////////////////////////////
function res_menu_num2mois($num){
	setlocale(LC_TIME, 'fr', 'fr_FR.UTF-8'); /* à internationaliser...*/
	$ret=utf8_encode(strftime('%B', mktime(0, 0, 0, $num, 1, 2000)));	
	return $ret;
}

//////////////////////////////////////////////////////
// converti une date en nom du jour dans la semaine //
//////////////////////////////////////////////////////
function res_menu_date2jour($j, $m, $a){
	setlocale(LC_TIME, 'fr', 'fr_FR.UTF-8'); /* à internationaliser...*/
	$ret=utf8_encode(strftime('%A', mktime(0, 0, 0, $m, $j, $a)));
	return $ret;	
}


// ******************************************************
// Function that returns the dates for each day in a week
// base trouvé sur : http://http://forum.hardware.fr/hfr/Programmation/PHP/date-semaine-donnee-sujet_97714_1.htm
// ******************************************************
function week_dates($week,$year,$wday) {
   // Get timestamp of first week of the year
   $first_day = mktime(12,0,0,1,1,$year);
   $first_week = date("W",$first_day);
   if ($first_week > 1) {
	   $first_day = strtotime("+1 week",$first_day); // skip to next if year does not begin with week 1
   }
   // Get timestamp of the week
   $timestamp = strtotime("+".($week-1)." week",$first_day);
   // Adjust to Monday of that week
   $what_day = date("w",$timestamp); // I wanted to do "N" but only version 4.3.9 is installed :-(
   if ($what_day==0) {
	   // actually Sunday, last day of the week. FIX;
	   $timestamp = strtotime("-6 days",$timestamp);
   } elseif ($what_day > 1) {
	   $what_day--;
	   $timestamp = strtotime("-$what_day days",$timestamp);
   }
   $ret=strtotime("+".($wday)." day",$timestamp);
   
   return $ret;
   
}



function lien_du_jour(){
	global $tab_req;
	setlocale(LC_TIME, 'fr', 'fr_FR.UTF-8'); /* à internationaliser...*/
	$label='<b>'.strftime('%A %d %B %Y')." (S".date("W").")</b>"; /* transformer en lien contextuel à l'affichage */
	$req="?aff=".$tab_req[0]."-";
	if($tab_req[0]=="S"){ // si semaine
		$req.=date("Y-W-").((date("w")+0)==0?7:date("w"));
	}else{
		$req.=date("Y-n-j");
	}
	$ret='<a href="'.$req.'">'.$label.'</a>';
	return $ret;
}


?>